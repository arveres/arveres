module.exports = {
  host: 'http://localhost:3000/',

  index: '',
  signup: 'signup',
  login: 'login',
  logout: 'logout',
  auth: {
    google: 'auth/google'
  },

  auctions: {
    search:'auctions/search',
    create:'auctions/create',
    delete: 'auctions/delete',
    bid: 'auctions/bid'
  },

  user:{
    profile:'user/profile',//profile
    myAuctions:'user/myauctions',
    subs:'user/subs',
    wins:'user/wins',
    edit:'user/edit'//edit
  }
};
