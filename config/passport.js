/**
 * @module arveres/config/passport
 */

var mongoose = require('mongoose');

var LocalStrategy = require('passport-local');
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

var User = require('../dao/user');
var auth = require('./auth');

function configurePassport(passport) {
  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
      done(err, user);
    });
  });

  var config = {
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
  };

  var signupStrategy = new LocalStrategy(config, function(req, email, password, done) {
    User.findByEmail(email, function(err, users) {
      if(err) {
        return done(err);
      }

      if(users.length) {
        return done(null, false, req.flash('message', 'User exists'));
      }

      var hash = User.createHash(password)
      User.create(req.body.name, hash,
                  email, function(err, id) {
        if(err) {
          return done(err);
        }

        var user = {
          id: id,
          email: email,
          name: req.body.name
        };

        done(null, user);
      });
    });
  });

  var loginStrategy = new LocalStrategy(config, function(req, email, password, done) {
    User.findByEmail(email, function(err, users) {
      if(err) {
        return done(err);
      }

      if(!users.length) {
        return done(null, false, req.flash('message', 'User does not exist'));
      }

      if(!User.validate(password, users[0].password)) {
        return done(null, false, req.flash('message', 'Incorrect password'));
      }

      done(null, users[0]);
    });
  });

  var googleStrategy = new GoogleStrategy(
    {
      clientID: auth.google.clientID,
      clientSecret: auth.google.clientSecret,
      callbackURL: auth.google.callbackURL
    },
    function(accessToken, refreshToken, profile, done){

      //wait until we have all data back from google
      process.nextTick(function(){
        User.findByEmail(profile.emails[0].value, function(err, rows){
          if(err){
            return done(err);
          }

          //if user doesn't already exist
          if(!rows || (rows.length === 0)){

            //create new user
            User.create(profile.displayName, '', profile.emails[0].value, profile.id, function(err, id){
              if(err){
                return done(err);
              }

              var user = {
                id: id,
                email: profile.emails[0].value,
                profileid: profile.id
              };
              return done(null, user);
            });
          }

          //extra security check to make sure profile.id matches the stored one
          if(rows[0].profileid != profile.id){
            return done(null, false, {message: 'Security Error: profile id of Google Sign-in and stored profile id don\'t match'});
          }

          var user = {
            id: rows[0].id,
            email: rows[0].email,
            profileid: rows[0].profileid
          };
          return done(null, user);
        });
      });
    }
  );

  passport.use('google', googleStrategy);
  passport.use('local-signup', signupStrategy);
  passport.use('local-login', loginStrategy);
}


module.exports = configurePassport;
