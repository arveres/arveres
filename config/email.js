/**
 * @module arveres/config/email
 */

// this is example data and is subject to change
module.exports = {
  from: 'arveres.auctions@gmail.com',

  win: {
    seller: {
      subject: 'Auction Over!',
      body: function(name, item) {
        return 'Good News, ' + name + '!' +
               'Your ' + item + ' has been sold!';
      }
    },

    buyer: {
      subject: 'Auction Winner',
      body: function(name, item) {
        return 'Congratulations ' + name +
               ' you have won the ' + item + '!!\n' +
               'Please go to you profile for more information.';
      }
    }
  },

  lose: {
    subject: 'Our Condolences',
    body: function(name, item) {
      return 'We are sorry to inform you, ' + name +
             ', that your ' + item + ' has not sold' +
             ' by the end of its auction';
    }
  }
};
