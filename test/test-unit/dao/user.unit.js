
'use strict';

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');
var proxyquire = require('proxyquire');
var sinonChai = require('sinon-chai');

chai.use(sinonChai);

var db = {
  insert: function(){},
  select: function(){
    return this;
  },
  where: function(){
    return this;
  },
  exec: function(){}
};

var bcrypt = {
  compareSync: function(){},
  genSaltSync: function(input){
    return ' with salt';
  },
  hashSync: function(password, salt){
    return password + salt;
  }
};

var User = proxyquire('../../../dao/user', {
  '../db': {
    db: db
  },
  'bcrypt': bcrypt
});

describe('User', function(){
  describe('.create', function(){
    var sandbox;

    before(function(){
      sandbox = sinon.sandbox.create();

      sandbox.stub(User, 'populate', function(cb, err, rows){
        if(err){
          return cb(err);
        }
        return cb(null, [{id: 'id'}]);
      });
    });

    afterEach(function(){
      sandbox.reset();
    });

    after(function(){
      sandbox.restore();
    });

    it('should insert a new row into users', function(done){
      var i = sandbox.stub(db, 'insert', function(table, entry, cb){
        cb();
      });

      User.create('name', 'password', 'email', 'profileid', 'token', 'refresh', function(){
        expect(db.insert).to.have.been.calledWith('users');
        i.restore();
        done();
      });
    });

    it('should pass back an error when insertion fails', function(done){
      var i = sandbox.stub(db, 'insert', function(table, entry, cb){
        cb(new Error('error'));
      });

      User.create('name', 'password', 'email', 'profileid', 'token', 'refresh', function(err){
        expect(err).to.be.ok;
        expect(err.message).to.equal('error');
        i.restore();
        done();
      });
    });

    it('should function the same whether or not profileid is specified', function(done){
      var i = sandbox.stub(db, 'insert', function(table, entry, cb){
        cb(entry);
      });

      var entry = {
        name: 'name',
        password: 'password',
        email: 'email',
        profileid: 'profileid',
        token: 'token',
        refresh: 'refresh'
      };
      User.create(entry.name, entry.password, entry.email, entry.profileid, entry.token, entry.refresh, function(withpid){
        User.create(entry.name, entry.password, entry.email, function(withoutpid){
          expect(withoutpid.name).to.equal(withpid.name);
          expect(withoutpid.password).to.equal(withpid.password);
          expect(withoutpid.email).to.equal(withpid.email);
          i.restore();
          done();
        });
      });
    });
  });

  describe('.findById', function(){
    var sandbox;

    before(function(){
      sandbox = sinon.sandbox.create();
      sandbox.spy(db, 'select');
      sandbox.spy(db, 'where');
    });

    afterEach(function(){
      sandbox.reset();
    });

    after(function(){
      sandbox.restore();
    });

    it('should select from users', function(done){
      var e = sandbox.stub(db, 'exec', function(cb){
        cb(null, [{
          get: function(){
            return 'id';
          }
        }]);
      });

      User.findById('0', function(){
        expect(db.select).to.have.been.calledWith('users');
        e.restore();
        done();
      });
    });

    it('should select only users with a specified id', function(done){
      var e = sandbox.stub(db, 'exec', function(cb){
        cb(null, [{
          get: function(){
            return 'id';
          }
        }]);
      });

      User.findById('0', function(){
        expect(db.where).to.have.been.called;
        expect(db.where.args[0][0].id).to.equal('0');
        e.restore();
        done();
      });
    });

    it('should pass back an error if selection fails', function(done){
      var e = sandbox.stub(db, 'exec', function(cb){
        cb(new Error('error'));
      });

      User.findById('0', function(err){
        expect(err).to.be.ok;
        expect(err.message).to.equal('error');
        e.restore();
        done();
      });
    });
  });

  describe('.findByProfileId', function(){
    var sandbox;

    before(function(){
      sandbox = sinon.sandbox.create();
      sandbox.spy(db, 'select');
      sandbox.spy(db, 'where');
    });

    afterEach(function(){
      sandbox.reset();
    });

    after(function(){
      sandbox.restore();
    });

    it('should select from users', function(done){
      var e = sandbox.stub(db, 'exec', function(cb){
        cb(null, [{
          get: function(){
            return 'id';
          }
        }]);
      });

      User.findByProfileId('0', function(){
        expect(db.select).to.have.been.calledWith('users');
        e.restore();
        done();
      });
    });

    it('should select only users with a specified id', function(done){
      var e = sandbox.stub(db, 'exec', function(cb){
        cb(null, [{
          get: function(){
            return 'id';
          }
        }]);
      });

      User.findByProfileId('0', function(){
        expect(db.where).to.have.been.called;
        expect(db.where.args[0][0].profileid).to.equal('0');
        e.restore();
        done();
      });
    });

    it('should pass back an error if selection fails', function(done){
      var e = sandbox.stub(db, 'exec', function(cb){
        cb(new Error('error'));
      });

      User.findByProfileId('0', function(err){
        expect(err).to.be.ok;
        expect(err.message).to.equal('error');
        e.restore();
        done();
      });
    });
  });

  describe('.findByEmail', function(){
    var sandbox;

    before(function(){
      sandbox = sinon.sandbox.create();
      sandbox.spy(db, 'select');
      sandbox.spy(db, 'where');
    });

    afterEach(function(){
      sandbox.reset();
    });

    after(function(){
      sandbox.restore();
    });

    it('should select from users', function(done){
      var e = sandbox.stub(db, 'exec', function(cb){
        cb(null, [{
          get: function(){
            return 'id';
          }
        }]);
      });

      User.findByEmail('test@site.com', function(){
        expect(db.select).to.have.been.calledWith('users');
        e.restore();
        done();
      });
    });

    it('should select only users with the given email', function(done){
      var e = sandbox.stub(db, 'exec', function(cb){
        cb(null, [{
          get: function(){
            return 'id';
          }
        }]);
      });

      User.findByEmail('test@site.com', function(){
        expect(db.where).to.have.been.called;
        expect(db.where.args[0][0].email).to.equal('test@site.com');
        e.restore();
        done();
      });
    });

    it('should pass back an error if selection fails', function(done){
      var e = sandbox.stub(db, 'exec', function(cb){
        cb(new Error('error'));
      });

      User.findByEmail('test@site.com', function(err){
        expect(err).to.be.ok;
        expect(err.message).to.equal('error');
        e.restore();
        done();
      });
    });
  });

  describe('.findByName', function(){
    var sandbox;

    before(function(){
      sandbox = sinon.sandbox.create();
      sandbox.spy(db, 'select');
      sandbox.spy(db, 'where');
    });

    afterEach(function(){
      sandbox.reset();
    });

    after(function(){
      sandbox.restore();
    });

    it('should select from users', function(done){
      var e = sandbox.stub(db, 'exec', function(cb){
        cb(null, [{
          get: function(){
            return 'id';
          }
        }]);
      });

      User.findByName('username', function(){
        expect(db.select).to.have.been.calledWith('users');
        e.restore();
        done();
      });
    });

    it('should select only users with the given username', function(done){
      var e = sandbox.stub(db, 'exec', function(cb){
        cb(null, [{
          get: function(){
            return 'id';
          }
        }]);
      });

      User.findByName('username', function(){
        expect(db.where).to.have.been.called;
        expect(db.where.args[0][0].name).to.equal('username')
        e.restore();
        done();
      });
    });

    it('should pass back an error if selection fails', function(done){
      var e = sandbox.stub(db, 'exec', function(cb){
        cb(new Error('error'));
      });

      User.findByName('username', function(err){
        expect(err).to.be.ok;
        expect(err.message).to.equal('error');
        e.restore();
        done();
      });
    });
  });

  describe('.validate', function(){
    var sandbox;

    before(function(){
      sandbox = sinon.sandbox.create();
    });

    afterEach(function(){
      sandbox.reset();
    });

    after(function(){
      sandbox.restore();
    });

    it('should compare a password to a hash', function(done){
      var c = sandbox.stub(bcrypt, 'compareSync', function(){});

      User.validate('password', 'hash');
      expect(bcrypt.compareSync).to.have.been.calledWith('password', 'hash');
      c.restore();
      done();
    });

    it('should return true if comparison returns true', function(done){
      var c = sandbox.stub(bcrypt, 'compareSync', function(){
        return true;
      });

      expect(User.validate('one', 'two')).to.equal(true);
      c.restore();
      done();
    });

    it('should return false if comparison returns false', function(done){
      var c = sandbox.stub(bcrypt, 'compareSync', function(){
        return false;
      });

      expect(User.validate('one', 'two')).to.equal(false);
      c.restore();
      done();
    });
  });
});
