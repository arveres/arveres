
'use strict';

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');
var proxyquire = require('proxyquire');

var sinonChai = require('sinon-chai');
chai.use(sinonChai);

var db = {
  insert: function() {},

  select: function() {
    return this;
  },

  where: function() {
    return this;
  },

  orderBy: function() {
    return this;
  },

  limit: function() {
    return this;
  },

  exec: function() {}
};

var Auction = proxyquire('../../../dao/auction', {
  '../db': {
    db: db
  }
});

describe('Auction', function() {
  describe('.create', function() {
    var sandbox;

    before(function() {
      sandbox = sinon.sandbox.create();
    });

    after(function() {
      sandbox.restore();
    });

    afterEach(function() {
      sandbox.reset();
    });

    it('should insert a new row into auctions', function(done) {
      var i = sandbox.stub(db, 'insert', function(table, entry, cb) {
        cb();
      });

      Auction.create('sellerId', 'itemId', 'title', 50, 100, new Date(), function() {
        expect(db.insert).to.have.been.calledWith('auctions');
        i.restore();
        done();
      });
    });

    it('should pass an error back if error fails', function(done) {
      var i = sandbox.stub(db, 'insert', function(table, entry, cb) {
        cb(new Error('error'));
      });

      Auction.create('sellerId', 'itemId', 'title', 50, 100, new Date(), function(err) {
        expect(err).to.be.ok;
        expect(err.message).to.equal('error');
        i.restore();
        done();
      });
    });
  });

  describe('.subscribe', function() {
    var sandbox;

    before(function() {
      sandbox = sinon.sandbox.create();
    });

    after(function() {
      sandbox.restore();
    });

    afterEach(function() {
      sandbox.reset();
    });

    it('should insert a new row into auctions', function(done) {
      var i = sandbox.stub(db, 'insert', function(table, entry, cb) {
        cb();
      });

      Auction.subscribe('userId', 'auctionId', function() {
        expect(db.insert).to.have.been.calledWith('hasSubscription');
        i.restore();
        done();
      });
    });

    it('should pass an error back if error fails', function(done) {
      var i = sandbox.stub(db, 'insert', function(table, entry, cb) {
        cb(new Error('error'));
      });

      Auction.subscribe('userId', 'auctionId', function(err) {
        expect(err).to.be.ok;
        expect(err.message).to.equal('error');
        i.restore();
        done();
      });
    });
  });

  describe('.hasSubscription', function() {
    var sandbox;

    before(function(){
      sandbox = sinon.sandbox.create();
      sandbox.spy(db, 'where');
      sandbox.spy(db, 'select');
    });

    after(function(){
      sandbox.restore();
    });

    afterEach(function(){
      sandbox.reset();
    });

    it('should select from auctions', function(done){
      var e = sandbox.stub(db, 'exec', function(cb){
        cb([{id: 'id'}]);
      });

      Auction.hasSubscription('userId', 'auctionId', function(){
        expect(db.select).to.have.been.calledWith('hasSubscription');
        e.restore();
        done();
      });
    });

    it('should select only specified ids', function(done){
      var e = sandbox.stub(db, 'exec', function(cb){
        cb([{id: 'id'}]);
      });

      Auction.hasSubscription('userId', 'auctionId', function(){
        expect(db.where).to.have.been.called;
        expect(db.where.args[0][0].userId).to.equal('userId');
        expect(db.where.args[0][0].auctionId).to.equal('auctionId');
        e.restore();
        done();
      });
    });

    it('should pass back an error if selection fails', function(done){
      var e = sandbox.stub(db, 'exec', function(cb){
        cb(new Error('error'));
      });

      Auction.hasSubscription('userId', 'auctionId', function(err){
        expect(err).to.be.ok;
        expect(err.message).to.equal('error');
        e.restore();
        done();
      });
    });
  });

  describe('.findAll', function() {
    var sandbox;

    before(function(){
      sandbox = sinon.sandbox.create();
      sandbox.spy(db, 'select');
    });

    after(function(){
      sandbox.restore();
    });

    afterEach(function(){
      sandbox.reset();
    });

    it('should select from auctions', function(done){
      var e = sandbox.stub(db, 'exec', function(cb){
        cb([{id: 'id'}]);
      });

      Auction.findAll(function(){
        expect(db.select).to.have.been.calledWith('auctions');
        e.restore();
        done();
      });
    });

    it('should pass back an error if selection fails', function(done){
      var e = sandbox.stub(db, 'exec', function(cb){
        cb(new Error('error'));
      });

      Auction.findAll(function(err){
        expect(err).to.be.ok;
        expect(err.message).to.equal('error');
        e.restore();
        done();
      });
    });
  });

  describe('.findById', function(){
    var sandbox;

    before(function(){
      sandbox = sinon.sandbox.create();
      sandbox.spy(db, 'where');
      sandbox.spy(db, 'select');
    });

    after(function(){
      sandbox.restore();
    });

    afterEach(function(){
      sandbox.reset();
    });

    it('should select from auctions', function(done){
      var e = sandbox.stub(db, 'exec', function(cb){
        cb([{id: 'id'}]);
      });

      Auction.findById('0', function(){
        expect(db.select).to.have.been.calledWith('auctions');
        e.restore();
        done();
      });
    });

    it('should select only specified ids', function(done){
      var e = sandbox.stub(db, 'exec', function(cb){
        cb([{id: 'id'}]);
      });

      Auction.findById('0', function(){
        expect(db.where).to.have.been.called;
        expect(db.where.args[0][0].id).to.equal('0');
        e.restore();
        done();
      });
    });

    it('should pass back an error if selection fails', function(done){
      var e = sandbox.stub(db, 'exec', function(cb){
        cb(new Error('error'));
      });

      Auction.findById('0', function(err){
        expect(err).to.be.ok;
        expect(err.message).to.equal('error');
        e.restore();
        done();
      });
    });
  });

  describe('.findBySeller', function(){
    var sandbox;

    before(function(){
      sandbox = sinon.sandbox.create();
      sandbox.spy(db, 'where');
      sandbox.spy(db, 'select');

      sandbox.stub(Auction, 'populate', function(cb, err, rows){
        if(err){
          return cb(err);
        }
        return cb();
      });
    });

    after(function(){
      sandbox.restore();
    });

    afterEach(function(){
      sandbox.reset();
    });

    it('should select from auctions', function(done){
      var e = sandbox.stub(db, 'exec', function(cb){
        cb(null, [{
          get: function(){
            return 'id';
          }
        }]);
      });

      Auction.findBySeller('sellerId', function(){
        expect(db.select).to.have.been.calledWith('auctions');
        e.restore();
        done();
      });
    });

    it('should select only specified ids', function(done){
      var e = sandbox.stub(db, 'exec', function(cb){
        cb(null, [{
          get: function(fieldname){
            return fieldname;
          }
        }]);
      });

      Auction.findBySeller('sellerId', function(){
        expect(db.where).to.have.been.called;
        expect(db.where.args[0][0].sellerId).to.equal('sellerId');
        e.restore();
        done();
      });
    });

    it('should pass back an error if selection fails', function(done){
      var e = sandbox.stub(db, 'exec', function(cb){
        cb(new Error('error'));
      });

      Auction.findBySeller('sellerId', function(err){
        expect(err).to.be.ok;
        expect(err.message).to.equal('error');
        e.restore();
        done();
      });
    });
  });

  //findByItem is not yet an implemented function in Auction
  // describe('.findByItem', function(){
  //
  //   // notice none of these tests run
  //   // this is due to auction not knowing itemId
  //   // please keep this temporarily just in case
  //   var sandbox;
  //
  //   before(function(){
  //     sandbox = sinon.sandbox.create();
  //     sandbox.spy(db, 'where');
  //     sandbox.spy(db, 'select');
  //   });
  //
  //   after(function(){
  //     sandbox.restore();
  //   });
  //
  //   afterEach(function(){
  //     sandbox.reset();
  //   });
  //
  //   xit('should select from auctions', function(done){
  //     var p = sandbox.stub(Auction, 'populate', function(cb, err, rows){
  //       if(err){
  //         return cb(err);
  //       }
  //       return cb();
  //     });
  //
  //     var e = sandbox.stub(db, 'exec', function(cb){
  //       cb(null, [{
  //         get: function(){
  //           return 'id';
  //         }
  //       }]);
  //     });
  //
  //     Auction.findByItem('itemId', function(){
  //       expect(db.select).to.have.been.calledWith('auctions');
  //       e.restore();
  //       p.restore();
  //       done();
  //     });
  //   });
  //
  //   xit('should select only specified ids', function(done){
  //     var e = sandbox.stub(db, 'exec', function(cb){
  //       cb(null, [{
  //         get: function(){
  //           return 'id';
  //         }
  //       }]);
  //     });
  //
  //     Auction.findByItem('itemId', function(){
  //       expect(db.where).to.have.been.called;
  //       expect(db.where.args[0][0].itemId).to.equal('itemId');
  //       e.restore();
  //       done();
  //     });
  //   });
  //
  //   xit('should pass back an error if selection fails', function(done){
  //     var e = sandbox.stub(db, 'exec', function(cb){
  //       cb(new Error('error'));
  //     });
  //
  //     Auction.findByItem('itemId', function(err){
  //       expect(err).to.be.ok;
  //       expect(err.message).to.equal('error');
  //       e.restore();
  //       done();
  //     });
  //   });
  // });

  describe('.findByTitle', function(){
    var sandbox;

    before(function(){
      sandbox = sinon.sandbox.create();
      sandbox.spy(db, 'where');
      sandbox.spy(db, 'select');

      sandbox.stub(Auction, 'populate', function(cb, err, rows){
        if(err){
          return cb(err);
        }
        return cb();
      });
    });

    after(function(){
      sandbox.restore();
    });

    afterEach(function(){
      sandbox.reset();
    });

    it('should select from auctions', function(done){
      var e = sandbox.stub(db, 'exec', function(cb){
        cb(null, [{
          get: function(){
            return 'id';
          }
        }]);
      });

      Auction.findByTitle('title', function(){
        expect(db.select).to.have.been.calledWith('auctions');
        e.restore();
        done();
      });
    });

    it('should select only specified ids', function(done){
      var e = sandbox.stub(db, 'exec', function(cb){
        cb(null, [{
          get: function(){
            return 'id';
          }
        }]);
      });

      Auction.findByTitle('title', function(){
        expect(db.where).to.have.been.called;
        expect(db.where.args[0][0].title).to.equal('title');
        e.restore();
        done();
      });
    });

    it('should pass back an error if selection fails', function(done){
      var e = sandbox.stub(db, 'exec', function(cb){
        cb(new Error('error'));
      });

      Auction.findByTitle('title', function(err){
        expect(err).to.be.ok;
        expect(err.message).to.equal('error');
        e.restore();
        done();
      });
    });
  });

  describe('.findByKeyword', function(){
    var sandbox;
    var fbi;

    before(function(){
      sandbox = sinon.sandbox.create();
      sandbox.spy(db, 'where');
      sandbox.spy(db, 'select');

      fbi = sandbox.stub(Auction, 'findById', function(id, cb) {
        cb(null, {get: function() {return 'auctionId';}});
      });
    });

    after(function(){
      sandbox.restore();
    });

    afterEach(function(){
      sandbox.reset();
    });

    it('should select from auctions', function(done){
      var e = sandbox.stub(db, 'exec', function(cb){
        cb(null, [{
          get: function() {
            return 'auctionId';
          }
        }]);
      });

      Auction.findByKeyword('keywordId', function(){
        expect(db.select).to.have.been.calledWith('hasKeyword');
        e.restore();
        done();
      });
    });

    it('should select only specified ids', function(done){
      var e = sandbox.stub(db, 'exec', function(cb){
        cb(null, [{
          get: function() {
            return 'auctionId';
          }
        }]);
      });

      Auction.findByKeyword('keywordId', function(){
        expect(db.where).to.have.been.called;
        expect(db.where.args[0][0].keywordId).to.equal('keywordId');
        e.restore();
        done();
      });
    });

    it('should map results to return Auctions', function(done) {
      var e = sandbox.stub(db, 'exec', function(cb){
        cb(null, [{
          get: function() {
            return 'auctionId';
          }
        }]);
      });

      Auction.findByKeyword('keywordId', function(err, rows){
        expect(rows).to.be.ok;
        expect(rows[0].get('id')).to.equal('auctionId');
        e.restore();
        done();
      });
    });

    it('should pass back an error if selection fails', function(done){
      var e = sandbox.stub(db, 'exec', function(cb){
        cb(new Error('error'));
      });

      Auction.findByKeyword('keywordId', function(err){
        expect(err).to.be.ok;
        expect(err.message).to.equal('error');
        e.restore();
        done();
      });
    });
  });

  describe('.findBySubscription', function(){
    var sandbox;
    var fbi;

    before(function(){
      sandbox = sinon.sandbox.create();
      sandbox.spy(db, 'where');
      sandbox.spy(db, 'select');

      fbi = sandbox.stub(Auction, 'findById', function(id, cb) {
        cb(null, {get: function() {return 'auctionId';}});
      });
    });

    after(function(){
      sandbox.restore();
    });

    afterEach(function(){
      sandbox.reset();
    });

    it('should select from auctions', function(done){
      var e = sandbox.stub(db, 'exec', function(cb){
        cb(null, [{
          get: function() {
            return 'auctionId';
          }
        }]);
      });

      Auction.findBySubscription('userId', function(){
        expect(db.select).to.have.been.calledWith('hasSubscription');
        e.restore();
        done();
      });
    });

    it('should select only specified ids', function(done){
      var e = sandbox.stub(db, 'exec', function(cb){
        cb(null, [{
          get: function() {
            return 'auctionId';
          }
        }]);
      });

      Auction.findBySubscription('userId', function(){
        expect(db.where).to.have.been.called;
        expect(db.where.args[0][0].userId).to.equal('userId');
        e.restore();
        done();
      });
    });

    it('should map results to return Auctions', function(done) {
      var e = sandbox.stub(db, 'exec', function(cb){
        cb(null, [{
          get: function() {
            return 'auctionId';
          }
        }]);
      });

      Auction.findBySubscription('userId', function(err, rows){
        expect(rows).to.be.ok;
        expect(rows[0].get('id')).to.equal('auctionId');
        e.restore();
        done();
      });
    });

    it('should pass back an error if selection fails', function(done){
      var e = sandbox.stub(db, 'exec', function(cb){
        cb(new Error('error'));
      });

      Auction.findBySubscription('userId', function(err){
        expect(err).to.be.ok;
        expect(err.message).to.equal('error');
        e.restore();
        done();
      });
    });
  });
});
