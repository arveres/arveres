'use strict';

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');
var proxyquire = require('proxyquire');

var sinonChai = require('sinon-chai');
chai.use(sinonChai);

var cronJob = sinon.spy();

var SystemController = proxyquire('../../system', {
  'cron': {
    CronJob: cronJob
  }
});

describe('SystemController', function() {
  describe('@constructor', function() {
    xit('should create an instance of SystemController', function() {
      expect(new SystemController()).to.be.instanceOf(SystemController);
    });

    xit('should initialize a `jobs` attribute', function() {
      var systemController = new SystemController();
      expect(systemController).to.have.property('jobs');
    });
  });

  describe('#createJob', function() {
    var sandbox;

    before(function() {
      sandbox = sinon.sandbox.create();
      sandbox.stub(SystemController, '_processJob');
      sandbox.stub(SystemController, '_deleteJob');
    });

    after(function() {
      sandbox.restore();
    });

    afterEach(function() {
      sandbox.reset();
      cronJob.reset();
    });

    xit('should construct a new cron job with the date provided', function() {
      var systemController = new SystemController();

      systemController.createJob('auction', 1);
      expect(cronJob).to.have.been.called;
    });

    xit('should add the new job to the `jobs` attribute', function() {
      var systemController = new SystemController();

      systemController.createJob('auction', 1);
      expect(systemController.jobs).to.have.property('auction');
    });

    xit('should throw an error if a job exists for the given auction', function() {
      var systemController = new SystemController();
      systemController.jobs.auction = 'job';

      expect(systemController.createJob.bind(systemController, 'auction', 1))
        .to.throw(Error, /Auction ID auction already exists/);
    });
  });

  describe('#deleteJob', function() {
    xit('should stop the job matching the given auction', function() {
      var systemController = new SystemController();
      var job = {
        stop: sinon.spy()
      };

      systemController.jobs.auction = job;

      systemController.deleteJob('auction');
      expect(job.stop).to.have.been.called;
    });

    xit('should throw an error if a job does not exist for the given auction', function() {
      var systemController = new SystemController();

      expect(systemController.deleteJob.bind(systemController, 'auction'))
        .to.throw(Error, /Auction ID auction does not exist/);
    });
  });

  describe('#_processJob', function() {

  });

  describe('#_deleteJob', function() {
    xit('should delete the job for the given auction from the `jobs` attribute', function() {
      var systemController = new SystemController();
      systemController.jobs['auction'] = 'job';

      systemController._deleteJob('auction');
      expect(systemController.jobs).to.not.have.property('auction');
    });
  });
});
