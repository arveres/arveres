'use strict';

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');
var proxyquire = require('proxyquire');

var sinonChai = require('sinon-chai');
chai.use(sinonChai);

var SystemController = require('../../system');

describe('SystemController', function() {
  describe('#createJob', function() {
    var sandbox;
    var clock;

    before(function() {
      sandbox = sinon.sandbox.create();
      clock = sinon.useFakeTimers();
    });

    after(function() {
      sandbox.restore();
      clock.restore();
    });

    afterEach(function() {
      sandbox.reset();
      clock.reset();
    });

    xit('should create a Cron Job that executes `_processJob` on date provided', function(done) {
      var pj = sandbox.stub(SystemController.prototype, '_processJob', function() {
        expect(pj).to.have.been.called;
        pj.restore();
        done();
      });

      var systemController = new SystemController();
      systemController.createJob('auction', 1000);
      clock.tick(1001);
    });
  });

  describe('#deleteJob', function() {
    var sandbox;
    var clock;

    before(function() {
      sandbox = sinon.sandbox.create();
      clock = sinon.useFakeTimers();
    });

    after(function() {
      sandbox.restore();
      clock.restore();
    });

    afterEach(function() {
      sandbox.reset();
      clock.reset();
    });

    xit('should stop the cro job specified by the `auctionId` provided', function(done) {
      var dj = sandbox.stub(SystemController.prototype, '_deleteJob', function() {
        expect(dj).to.have.been.called;
        dj.restore();
        done();
      });

      var systemController = new SystemController();
      systemController.createJob('auction', 1);
      systemController.deleteJob('auction');
    });
  });
});
