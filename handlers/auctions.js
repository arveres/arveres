/**
 * @module arveres/handlers/auctions
 */

'use strict';

var urlConfig = require('../config/url');
var systemController = require('../system');

var async = require('async');

var Bid = require('../dao/bid');
var Item = require('../dao/item');
var Auction = require('../dao/auction');
var Keyword = require('../dao/keyword');
var Notification = require('../dao/notification');

var AuctionsHandler = {
  getAuctions: getAuctions,
  getMyAuctions: getMyAuctions,
  getCreateAuction: getCreateAuction,
  postCreateAuction: postCreateAuction,
  postBid: postBid,
  getAuctionsByCategory:getAuctionsByCategory,
  deleteAuction: deleteAuction,
  getMyNotification: getMyNotification
}

/**
 * GET auctions by user id
 * @param {object} req
 * @param {object} res
 */
function getMyAuctions(req, res) {
  Auction.findBySeller(req.user.id, function(err, rows) {
    if(err) {
      console.log(err);
    }   
    res.send(rows);
  });
}

/**
 * Render Auction Category Page
 * @param {object} req
 * @param {object} res
 */
function getAuctionsByCategory(req, res) {
  var category = req.params.category;

  Auction.findByKeyword(category, function(err,rows){
    if(err) {
      console.log(err);
    }

    res.render('auctions/search', {
      title: 'Arveres',
      url: urlConfig,
      user: req.user,
      auctions : rows
    });
  });
}

/**
 * Render Auction Page
 * @param {object} req
 * @param {object} res
 */
function getAuctions(req, res) {
  var search = req.query.search;

  Auction.findAll(function(err,rows){
    if(err) {
      console.log(err);
    }

    if(search) {
      var regex = new RegExp('.*' + search + '.*', 'ig');

      rows = rows.filter(function(row){
        var found = row.title.match(regex);//Regular expression
        return found;
      });
    }

    res.render('auctions/search', {
      title: 'Arveres',
      url: urlConfig,
      user: req.user,
      auctions : rows
    });
  });
}

  function getMyNotification(req, res){
    if(req.user){
        Notification.findByUser(req.user.id, function(err,rows){
          if(err) {
            console.log(err);
          }

          res.send(rows);
        });
    }
    else{
      res.send([]);
    }
 }

/**
 * Render Create Page
 * @param {object} req
 * @param {object} res
 */
function getCreateAuction(req,res) {
  res.render('auctions/create',{
    title: 'Arveres',
    url: urlConfig,
    user: req.user
  });
}

/**
 *  Delete Auction
 *
 *  @param {object} req
 *  @param {object} res
 */
function deleteAuction(req, res){
  var auctionId = req.body.auctionId;

  async.series([
    Auction.deleteById.bind(null, auctionId),
    Auction.deleteSubscriptionByAuction.bind(null, auctionId),
    Keyword.deleteLinkByAuction.bind(null, auctionId)
  ], function(err) {
    if(err) {
      console.log(err);
    }

    res.redirect('/user/myauctions');
  });
}

/**
 * Create Auction
 * @param {object} req
 * @param {object} res
 */
function postCreateAuction(req,res) {
  var userId = req.user.id;

  var category = req.body.itemCategory;
  var itemName = req.body.itemName;
  var description = req.body.descriptionItem;
  var amount = req.body.amount;
  var picture = req.file.filename;

  var date = req.body.date;
  var hour = req.body.hour;
  var minutes = req.body.minutes;
  var fullTime = (new Date(date).getTime())+(60000*minutes)+(3600000*hour);

  async.waterfall([
    Item.create.bind(null, itemName, picture, description),
    Auction.create.bind(null, userId, itemName, amount, 500, fullTime),

    function(auctionId, done) {
      systemController.createJob(auctionId, fullTime);

      Keyword.link(category, auctionId, done);
    }

  ], function(err) {
    if(err) {
      console.log(err);
    }

    res.redirect('/user/profile');
  });
}
function postBid(req,res){
  var userId = req.user.id;

  var itemId = req.body.itemId;
  var auctionId = req.body.auctionId;
  var amount = req.body.amount;

  async.series([
    function(done) {
      async.waterfall([
        Bid.create.bind(null, userId, itemId, amount),
        Notification.create.bind(null, userId),
        systemController.createNotJob.bind(systemController)
      ], done);
    },

    function(done) {
      console.log('did that');
      Auction.hasSubscription(userId, auctionId, function(err, has) {
      console.log('got sub');
        if(err) {
          return done(err);
        }

        if(has) {
          return done();
        }

        Auction.subscribe(userId, auctionId, done)
      })
    }
  ], function(err) {
    console.log('outa there');
    if(err) {
      console.log(err);
    }

    res.redirect('/auctions/search');
  });
}

module.exports = AuctionsHandler;
