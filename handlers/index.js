/**
 * @module arveres/handlers/index
 */

'use strict';

var urlConfig = require('../config/url');

var IndexHandler = {
  getIndex:  getIndex,
  getLogout: getLogout
}

/**
 * Render Index Page
 * @param {object} req
 * @param {object} res
 */
function getIndex(req, res) {
  res.render('index', {
    title: 'Arveres',
    url: urlConfig,
    user: req.user
  })
}

/**
 * Logout User
 * @param {object} req
 * @param {object} res
 */
function getLogout(req, res) {
  req.logout();
  res.redirect('/');
}

module.exports = IndexHandler;
