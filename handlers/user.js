/**
 * @module arveres/handlers/user
 */

'use strict';

var urlConfig = require('../config/url');

var Bid = require('../dao/bid');
var Item = require('../dao/item');
var Auction = require('../dao/auction');
var User = require('../dao/user');
var Wins = require('../dao/win');

var UserHandler = {
  isLoggedIn: isLoggedIn,
  getUser: getUser,
  getMyAuctions: getMyAuctions,
  getSubs: getSubs,
  editProfile: editProfile,
  getWins: getWins,
  getInfo:getInfo//gone
}

/**
 * Determine if a user is logged in
 * @param {object} req
 * @param {object} res
 * @param {function} next
 */
function isLoggedIn(req, res, next) {
  if(req.isAuthenticated()) {
    return next();
  }

  res.redirect('/');
}

/**
 * Render Auction Page
 * @param {object} req
 * @param {object} res
 */
function getUser(req, res) {
  Auction.findBySeller(req.user.id, function(err,rows){
    if(err) {
      console.log(err);
    }
      res.render('user/edit', {//this was changed
      title: 'Arveres',
      url: urlConfig,
      user: req.user,
      message: req.flash('editMessage')
    });  
  });
}

/**
 * Render My Auctions Page
 * @param {object} req
 * @param {object} res
 */
function getMyAuctions(req, res) {
  Auction.findBySeller(req.user.id, function(err,rows){
    if(err) {
      console.log(err);
    }

    res.render('user/myauctions', {
      title: 'Arveres',
      url: urlConfig,
      user: req.user,
      myAuction: true,
      auctions:rows
    });
   
  });
}

function getWins(req, res) { 
  var userId = req.user.id;
  
  Wins.findByWinner(userId,function(err,rows){
    console.log(rows)
    if(err) {
      console.log(err);
    }
    
    res.render('user/wins', {
      title: 'Arveres',
      url: urlConfig,
      user: req.user,
      myAuction: true,
      wins:rows
    });
  });
}

/**
 * Render My Subscriptions Page
 * @param {object} req
 * @param {object} res
 */
function getSubs(req, res) {
  Auction.findBySubscription(req.user.id, function(err,rows){
    if(err) {
      console.log(err);
    }

    res.render('user/subs', {
      title: 'Arveres',
      url: urlConfig,
      user: req.user,
      auctions: rows
    });
  });
}

/**
 * Edit Profile
 * @param {object} req
 * @param {object} res
 */
function editProfile(req,res){
  
  var name = req.body.userName;
  var oldPassword = req.body.oldPassword;
  var newPassword = req.body.newPassword;

  var entry = {};

  if(name) {
    entry.name = name;
  }

  if(req.file) {
    entry.image = req.file.filename;
  }

  if(newPassword) {
    var newPassword = User.createHash(newPassword);

    if(User.validate(oldPassword, req.user.password)) {
      entry.password = newPassword;
    } else {
      console.log('tried: ', newPassword);
      req.flash('editMessage', 'Incorrect Password');
    }
  }

  User.update(req.user.id, entry, function(err) {
    if(err) {
      console.log(err);
    }

    res.redirect('/user/edit');
  });

}
function getInfo(req, res) {//gone
  Auction.findBySeller(req.user.id, function(err,rows){
    if(err) {
      console.log(err);
    }     
	res.render('user/profile', {
      title: 'Arveres',
      url: urlConfig,
      user: req.user,
      auctions:rows
    });  
  });
}
module.exports = UserHandler;

