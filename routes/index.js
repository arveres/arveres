/**
 * @module arveres/routes/index
 */

'use strict';

var Router = require('express').Router;

/**
 * Creates and returns the index router
 * @param {object} indexHandler
 * @return {object}
 */
function getIndexRouter(indexHandler, passport) {
  var router = Router();

  router.get('/', indexHandler.getIndex);
  router.get('/logout', indexHandler.getLogout);

  var config = {
    successRedirect: '/user/profile',
    failureRedirect: '/',
    failureFlash: true
  };

  router.post('/signup',
              passport.authenticate('local-signup', config));
  router.post('/login',
              passport.authenticate('local-login', config));

  router.get('/auth/google',
             passport.authenticate('google', {
               scope: [
                 'https://www.googleapis.com/auth/plus.me',
                 'https://www.googleapis.com/auth/plus.login',
                 'https://www.googleapis.com/auth/plus.profile.emails.read'
               ]
             }));

  router.get('/auth/google/callback',
            passport.authenticate('google',
                                  {
                                    successRedirect: '/user/profile',
                                    failureRedirect: '/'
                                  }));

  return router;
}

module.exports = getIndexRouter;
