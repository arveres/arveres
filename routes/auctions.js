/**
 * @module arveres/routes/auctions
 */

'use strict';

var Router = require('express').Router;

/**
 * Creates and returns the auctions router
 * @param {object} auctionsHandler
 * @return {object}
 */
function getAuctionsRouter(auctionsHandler) {
  var router = Router();

  router.get('/search', auctionsHandler.getAuctions);
  router.get('/search/:category', auctionsHandler.getAuctionsByCategory);
  router.get('/create', auctionsHandler.getCreateAuction);
  router.post('/create', auctionsHandler.postCreateAuction);
  router.post('/bid', auctionsHandler.postBid);
  router.post('/delete', auctionsHandler.deleteAuction);
  
  router.get('/get/mynotifications',auctionsHandler.getMyNotification);
  router.get('/get/myauctions', auctionsHandler.getMyAuctions);

  return router;
}

module.exports = getAuctionsRouter;
