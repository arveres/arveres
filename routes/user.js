/**
 * @module arveres/routes/user
 */

'use strict';

var Router = require('express').Router;

/**
 * Creates and returns the user router
 * @param {object} userHandler
 * @return {object}
 */
function getUserRouter(userHandler) {
  var router = Router();

  router.get('/edit', userHandler.isLoggedIn, userHandler.getUser);//profile
  router.get('/myauctions', userHandler.isLoggedIn,
             userHandler.getMyAuctions);
  router.get('/subs', userHandler.isLoggedIn, userHandler.getSubs);
  router.get('/wins', userHandler.isLoggedIn, userHandler.getWins);
 // router.get('/profile', userHandler.isLoggedIn, userHandler.getInfo);

  router.post('/edit', userHandler.isLoggedIn, userHandler.editProfile);//profile


  return router;
}

module.exports = getUserRouter;

