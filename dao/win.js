/**
 * @module arveres/dao/win
 */

'use strict';

var Database = require('../db');
var ObjectId = require('mongoose').mongo.ObjectID;

var async = require('async');

var User = require('./user');
var Item = require('./item');

var Win = {
  create: create,
  findById: findById,
  findByItem: findByItem,
  findByWinner: findByWinner,
  findBySeller: findBySeller,
  populate: populate
};

/**
 * Creates a win entry in the database
 * @param {string} winnerId
 * @param {string} sellerId
 * @param {string} itemId
 * @param {function} cb
 */
function create(winnerId, sellerId, itemId, cb) {
  var entry = {
    id: new ObjectId().toString(),
    winnerId: winnerId,
    sellerId: sellerId,
    itemId: itemId
  };

  Database.db.insert('wins', entry, cb);
}

/**
 * Finds win row by id
 * @param {string} id
 * @param {function} cb
 */
function findById(id, cb) {
  Database.db
    .select('wins')
    .where({id: id})
    .exec(Win.populate.bind(null, function(err, rows) {
      if(err) {
        return cb(err);
      }

      cb(null, rows[0]);
    }));
}

/**
 * Finds win row by item
 * @param {string} itemId
 * @param {function} cb
 */
function findByItem(itemId, cb) {
  Database.db
    .select('wins')
    .where({itemId: itemId})
    .exec(Win.populate.bind(null, cb));
}

/**
 * Finds win rows by winner
 * @param {string} winnerId
 * @param {function} cb
 */
function findByWinner(winnerId, cb) {
  Database.db
    .select('wins')
    .where({winnerId: winnerId})
    .exec(Win.populate.bind(null, cb));
}

/**
 * Finds win rows by seller
 * @param {string} sellerId
 * @param {function} cb
 */
function findBySeller(sellerId, cb) {
  Database.db
    .select('wins')
    .where({sellerId: sellerId})
    .exec(Win.populate.bind(null, cb));
}

/**
 * Populates wins with hold appropriate relations
 * @param {object} err
 * @param {array} rows
 */
function populate(cb, err, rows) {
  if(err) {
    return cb(err);
  }

  rows = rows.map(function(row) {
    return {
      id: row.get('id'),
      winnerId: row.get('winnerId'),
      sellerId: row.get('sellerId'),
      itemId: row.get('itemId')
    };
  });

  async.each(rows, function(row, done) {
    async.series({
      winner: User.findById.bind(null, row.winnerId),
      seller: User.findById.bind(null, row.sellerId),
      item: Item.findById.bind(null, row.itemId)
    }, function(err, results) {
      if(err) {
        return done(err);
      }

      row.winner = results.winner;
      row.seller = results.seller;
      row.item = results.item;

      done();
    });
  }, function(err) {
    if(err) {
      return cb(err);
    }

    cb(null, rows);
  });
}

module.exports = Win;
