/**
 * @module arveres/dao/auction
 */

'use strict';

var Database = require('../db');
var ObjectId = require('mongoose').mongo.ObjectID;

var Bid = require('./bid');
var Item = require('./item');
var User = require('./user');
var async = require('async');

var Auction = {
  create: create,
  deleteById: deleteById,
  subscribe: subscribe,
  hasSubscription: hasSubscription,
  deleteSubscriptionByAuction: deleteSubscriptionByAuction,
  findAll: findAll,
  findById: findById,
  findBySeller: findBySeller,
  findByTitle: findByTitle,
  findByKeyword: findByKeyword,
  findBySubscription: findBySubscription,
  populate: populate
};

/**
 * Creates an auction entry in database
 * @param {string} sellerId
 * @param {string} itemId not currently used
 * @param {string} title
 * @param {number} startingPrice
 * @param {number} reservePrice
 * @param {object} expiryDate
 * @param {function} cb
 */
function create(sellerId, title, startingPrice, reservePrice,
                expiryDate, itemId, cb) {
  var entry = {
    id: new ObjectId().toString(),
    sellerId: sellerId,
    itemId: itemId,
    title: title,
    startingPrice: startingPrice,
    reserverPrice:reservePrice,
    expiryDate: expiryDate
  };

  Database.db.insert('auctions', entry, function(err) {
    cb(err, entry.id);
  });
}

/**
 * Deletes an auction by id
 * @param {string} id
 * @param {function} cb
 */
function deleteById(id, cb) {
  Database.db
    .delete('auctions')
    .where({id: id})
    .exec(cb);
}

/**
 * Subscribes a user to a given auction
 * @param {string} userId
 * @param {string} auctionId
 * @param {function} cb
 */
function subscribe(userId, auctionId, cb) {
  var entry = {
    userId: userId,
    auctionId: auctionId
  };

  Database.db.insert('hasSubscription', entry, cb);
}

/**
 * Deletes subscriptions by auction
 * @param {string} id
 * @param {function} cb
 */
function deleteSubscriptionByAuction(id, cb) {
  Database.db
    .delete('hasSubscription')
    .where({auctionId: id})
    .exec(cb);
}

/**
 * Determines if the give user has a subscription to the given auction
 * @param {string} userId
 * @param {string} auctionId
 * @param {function} cb
 */
function hasSubscription(userId, auctionId, cb) {
  Database.db
    .select('hasSubscription')
    .where({userId: userId, auctionId: auctionId})
    .exec(function(err, rows) {
      if(err) {
        return cb(err);
      }

      cb(null, (rows.length > 0));
    });
}

/**
 * Find all auctionRows
 * @param {function} cb
 */
function findAll(cb) {
  Database.db
    .select('auctions')
    .exec(Auction.populate.bind(null, cb));
}

/**
 * Finds auction row by id
 * @param {string} id
 * @param {function} cb
 */
function findById(id, cb) {
  Database.db
    .select('auctions')
    .where({id: id})
    .exec(Auction.populate.bind(null, function(err, rows) {
      if(err) {
        return cb(err);
      }

      cb(null, rows[0]);
    }));
}

/**
 * Finds auction rows by seller
 * @param {string} sellerId
 * @param {function} cb
 */
function findBySeller(sellerId, cb) {
  Database.db
    .select('auctions')
    .where({sellerId: sellerId})
    .exec(Auction.populate.bind(null, cb));
}

/**
 * Find auction rows by title
 * @param {string} title
 * @param {function} cb
 */
function findByTitle(title, cb) {
  Database.db
    .select('auctions')
    .where({title: title})
    .exec(Auction.populate.bind(null, cb));
}

/**
 * Find auction rows by keyword
 * @param {string} keywordId
 * @param {function} cb
 */
function findByKeyword(keywordId, cb) {
  Database.db
    .select('hasKeyword')
    .where({keywordId: keywordId})
    .exec(function(err, rows) {
      if(err) {
        return cb(err);
      }

      rows = rows.map(function(row) {
        return row.get('auctionId');
      });

      async.map(rows, Auction.findById, cb);
    });
}

/**
 * Finds auction rows by subscription
 * @param {string} userId
 * @param {function} cb
 */
function findBySubscription(userId, cb) {
  Database.db
    .select('hasSubscription')
    .where({userId: userId})
    .exec(function(err, rows) {
      if(err) {
        return cb(err);
      }

      rows = rows.map(function(row) {
        return row.get('auctionId');
      });

      async.map(rows, Auction.findById, cb);
    });
}

/**
 * Populates auction itemId
 * @param {function} cb
 * @param {object} err
 * @param {array | object} rows
 */
function populate(cb, err, rows) {
  if(err) {
    return cb(err);
  }

  rows = rows.map(function(row) {
    return {
      id: row.get('id'),
      sellerId: row.get('sellerId'),
      itemId: row.get('itemId'),
      title: row.get('title'),
      startingPrice: row.get('startingPrice'),
      reserverPrice: row.get('reservePrice'),
      expiryDate: row.get('expiryDate')
    };
  });

  async.each(rows, function(row, done) {
    async.series({
      seller: User.findById.bind(null, row.sellerId),
      item: Item.findById.bind(null, row.itemId),
      bid: Bid.findTopByItem.bind(null, row.itemId)
    }, function(err, results) {
      if(err) {
        return done(err);
      }

      row.seller = results.seller;
      row.item = results.item;
      row.bid = results.bid;

      done();
    });
  }, function(err) {
    if(err) {
      return cb(err);
    }

    cb(null, rows);
  });
}

module.exports = Auction;
