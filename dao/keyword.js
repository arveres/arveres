/**
 * @module arveres/dao/keyword
 */

'use strict';

var Database = require('../db');
var ObjectId = require('mongoose').mongo.ObjectID;

var async = require('async');

var Keyword = {
  create: create,
  link: link,
  deleteLinkByAuction: deleteLinkByAuction,
  findById: findById,
  findByKeyword: findByKeyword,
  findByAuction: findByAuction,
  populate: populate
};

/**
 * Creates a keyword entry in database
 * @param {string} keyword
 * @param {function} cb
 */
function create(keyword, cb) {
  var entry = {
    id: new ObjectId().toString(),
    keyword: keyword
  };

  Database.db.insert('keywords', entry, cb);
}

/**
 * Links an auction to a keyword
 * @param {string} auctionId
 * @param {string} keywordId
 * @param {function} cb
 */
function link(keywordId, auctionId, cb) {
  var entry = {
    auctionId: auctionId,
    keywordId: keywordId
  };

  Database.db.insert('hasKeyword', entry, cb);
}

/**
 * Deletes link by auction
 * @param {string} auctionId
 * @param {function} cb
 */
function deleteLinkByAuction(auctionId, cb) {
  Database.db
    .delete('hasKeyword')
    .where({auctionId: auctionId})
    .exec(cb);
}

/**
 * Finds keyword row by id
 * @param {string} id
 * @param {function} cb
 */
function findById(id, cb) {
  Database.db
    .select('keywords')
    .where({id: id})
    .exec(Keyword.populate.bind(null, function(err, rows) {
      if(err) {
        return cb(err);
      }

      cb(null, rows[0])
    }));
}

/**
 * Finds keyword row by keyword
 * @param {string} keyword
 * @param {function} cb
 */
function findByKeyword(word, cb) {
  Database.db
    .select('keywords')
    .where({word: word})
    .exec(Keyword.populate.bind(null, cb));
}

/**
 * Finds keyword rows by auction
 * @param {string} auctionId
 * @param {function} cb
 */
function findByAuction(auctionId, cb) {
  Database.db
    .select('hasKeyword')
    .where({auctionId: auctionId})
    .exec(function(err, rows) {
      if(err) {
        return cb(err);
      }

      rows = rows.map(function(row) {
        row.get('keywordId');
      });

      async.map(rows, Keyword.findById, cb);
    });
}

/**
 * Populates keyword
 * @param {function} cb
 * @param {object} err
 * @param {array | object} rows
 */
function populate(cb, err, rows) {
  if(err)  {
    return cb(err);
  }

  rows = rows.map(function(row) {
    return {
      id: row.get('id'),
      keyword: row.get('keyword')
    };
  });

  cb(null, rows);
}

module.exports = Keyword;
