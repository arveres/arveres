/**
 *  @module arveres/dao/user
 */

'use strict';

var Database = require('../db');
var ObjectID = require('mongoose').mongo.ObjectID;
var bcrypt = require('bcrypt');

var User = {
  create: create,
  update: update,

  findById: findById,
  findByProfileId: findByProfileId,
  findByEmail: findByEmail,
  findByName: findByName,

  populate: populate,

  validate: validate,
  createHash: createHash
};

/**
 *  creates a new user
 *
 *  @param {string} name
 *  @param {string} password
 *  @param {string} email
 *  @param {string} [profileid] - the id for the google sign-in profile
 *  @param {function} cb - a callback function
 */
function create(name, password, email, profileid, token, refresh, cb){
  if(typeof profileid === 'function'){
    cb = profileid;
    profileid = '';
  }

  var user = {
    id: new ObjectID().toString(),
    name: name,
    password: password,
    email: email,
    profileid: profileid,
    token: token,
    refresh: refresh
  };

  Database.db.insert('users', user, function(err) {
    cb(err, user.id);
  });
}

/**
 * Updates a user by id
 * @param {string} id
 * @param {object} entry
 * @param {function} cb
 */
function update(id, entry, cb) {
  Database.db
    .update('users', entry)
    .where({id: id})
    .exec(cb);
}

/**
 *  finds users having the given id
 *
 *  @param {string} id
 *  @param {function} cb - a callback function
 */
function findById(id, cb){
  Database.db
    .select('users')
    .where({id: id})
    .exec(User.populate.bind(null, function(err, rows) {
      if(err) {
        return cb(err);
      }

      cb(null, rows[0]);
    }));
}

/**
 *  finds users having the given profileid
 *
 *  @param {string} profileid
 *  @param {function} cb - a callback function
 */
function findByProfileId(profileid, cb){
  Database.db
    .select('users')
    .where({profileid: profileid})
    .exec(User.populate.bind(null, function(err, rows) {
      if(err) {
        return cb(err);
      }

      cb(null, rows[0]);
    }));
}

/**
 *  finds users having the given email
 *
 *  @param {string} email
 *  @param {function} cb - a callback function
 */
function findByEmail(email, cb){
  Database.db
    .select('users')
    .where({email: email})
    .exec(User.populate.bind(null, cb));
}

/**
 *  finds users having the given username
 *
 *  @param {string} username
 *  @param {function} cb - a callback function
 */
function findByName(name, cb){
  Database.db
    .select('users')
    .where({name: name})
    .exec(User.populate.bind(null, cb));
}

/**
 * Populates user
 * @param {function} cb
 * @param {object} err
 * @param {array | object} rows
 */
function populate(cb, err, rows) {
  if(err)  {
    return cb(err);
  }

  rows = rows.map(function(row) {
    return {
      id: row.get('id'),
      name: row.get('name'),
      password: row.get('password'),
      email: row.get('email'),
      profileid: row.get('profileid'),
      token: row.get('token'),
      refresh: row.get('refresh'),
      image: row.get('image')
    };
  });

  cb(null, rows);
}

/**
 *  checks if a password is stored as the given hash
 *
 *  @param {string} password - the user's password
 *  @param {string} hash - the stored hash of a password
 */
function validate(password, hash){
  return bcrypt.compareSync(password, hash);
}

/**
 * User bcrypt to generate hash
 * @param {string} password
 * @returns {string}
 */
function createHash(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
}

module.exports = User;
