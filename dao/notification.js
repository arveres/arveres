/**
 * @module arveres/dao/notification
 */

'use strict';

var Database = require('../db');
var ObjectId = require('mongoose').mongo.ObjectID;

var Bid = require('./bid');
var User = require('./user');

var async = require('async');

var Notification = {
  create: create,
  deleteById: deleteById,

  findAll: findAll,
  findByUser: findByUser,
  populate: populate
};

/**
 * Creates a notifictation entry in the database
 * @param {string} userId
 * @param {string} bidId
 * @param {function} cb
 */
function create(userId, bidId, cb) {
  var entry = {
    id: new ObjectId().toString(),
    userId: userId,
    bidId: bidId,
    date: new Date().getTime()
  };

  Database.db.insert('notifications', entry, function(err) {
    if(err) {
      return cb(err);
    }

    cb(null, entry.id);
  });
}

/**
 * Deletes a notification by id
 * @param {string} id
 * @param {function} cb
 */
function deleteById(id, cb) {
  Database.db
    .delete('notifications')
    .where({id: id})
    .exec(cb);
}

/**
 * Retrieves all notifications in reverse date order
 * @param {function} cb
 */
function findAll(cb) {
  Database.db
    .select('notifications')
    .orderBy('date')
    .reverse()
    .exec(Notification.populate.bind(null, cb));
}

/**
 * Find rows by user
 * @param {string} userId
 * @param {function} cb
 */
function findByUser(userId, cb) {
  Database.db
    .select('notifications')
    .where({userId: userId})
    .orderBy('date')
    .reverse()
    .exec(Notification.populate.bind(null, cb));
}

/**
 * Populates notifications
 * @param {function} cb
 * @param {object} err
 * @param {array} rows
 */
function populate(cb, err, rows) {
  if(err) {
    return cb(err);
  }

  rows = rows.map(function(row) {
    return {
      id: row.get('id'),
      userId: row.get('userId'),
      bidId: row.get('bidId'),
      date: row.get('date')
    };
  });

  async.each(rows, function(row, done) {
    async.series({
      user: User.findById.bind(null, row.userId),
      bid: Bid.findById.bind(null, row.bidId)
    }, function(err, results) {
      if(err) {
        return done(err);
      }

      row.user = results.user;
      row.bid = results.bid;

      done();
    });
  }, function(err) {
    if(err) {
      return cb(err);
    }

    cb(null, rows);
  });
}

module.exports = Notification;

