/**
 *  @module arveres/dao/item
 */

'use strict';

var Database = require('../db');
var ObjectID = require('mongoose').mongo.ObjectID;

var Item = {
  create: create,
  findById: findById,
  findByTitle: findByTitle,
  populate: populate
}

/**
 *  Creates an Item entry in the database
 *
 *  @param {string} title - title of item being auctioned
 *  @param {string} image - name of image file
 *  @param {string} description - details of item
 *  @param {function} cb - a callback function
 */
function create(title, image, description, cb){
  var entry = {
    id: new ObjectID().toString(),
    title: title,
    image: image,
    description: description
  };

  Database.db.insert('items', entry, function(err) {
    cb(err, entry.id);
  });
}

/**
 *  Retrieves an Item row matching the id given
 *
 *  @param {string} id - the id of Item to retrieve
 *  @param {function} cb - a callback function
 */
function findById(id, cb){
  Database.db.select('items')
    .where({id: id})
    .exec(Item.populate.bind(null, function(err, rows) {
      if(err) {
        return cb(err);
      }

      cb(null, rows[0]);
    }));
}

/**
 *  Retrieves Items matching the specified title
 *
 *  @param {string} title - the title of the Item to retrieve
 *  @param {function} cb - a callback function
 */
function findByTitle(title, cb){
  Database.db.select('items')
    .where({title: title})
    .exec(Item.populate.bind(null, cb));
}

/**
 * Populates item
 * @param {function} cb
 * @param {object} err
 * @param {array | object} rows
 */
function populate(cb, err, rows) {
  if(err)  {
    return cb(err);
  }

  rows = rows.map(function(row) {
    return {
      id: row.get('id'),
      title: row.get('title'),
      image: row.get('image'),
      description: row.get('description')
    };
  });

  cb(null, rows);
}

module.exports = Item;
