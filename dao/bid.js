/**
 * @module arveres/dao/bid
 */

'use strict';

var Database = require('../db');
var ObjectId = require('mongoose').mongo.ObjectID;

var Item = require('./item');
var User = require('./user');

var async = require('async');

var Bid = {
  create: create,
  findById: findById,
  findByItem: findByItem,
  findTopByItem: findTopByItem,
  findByUser: findByUser,
  populate: populate
};

/**
 * Creates a bid entry in the database
 * @param {string} userId
 * @param {string} itemId
 * @param {function} cb
 */
function create(userId, itemId, amount, cb) {
  var entry = {
    id: new ObjectId().toString(),
    userId: userId,
    itemId: itemId,
    amount: amount,
    date: new Date().getTime()
  };

  Database.db.insert('bids', entry, function(err) {
    if(err) {
      return cb(err);
    }

    cb(null, entry.id)
  });
}

/**
 * Finds bid row by id
 * @param {string} id
 * @param {function} cb
 */
function findById(id, cb) {
  Database.db
    .select('bids')
    .where({id: id})
    .exec(Bid.populate.bind(null, function(err, rows) {
      if(err) {
        return cb(err);
      }

      cb(null, rows[0]);
    }));
}

/**
 * Finds bid rows by item
 * @param {string} itemId
 * @param {function} cb
 */
function findByItem(itemId, cb) {
  Database.db
    .select('bids')
    .where({itemId: itemId})
    .exec(Bid.populate.bind(null, cb));
}

/**
 * Finds top bid row by item
 * @param {string} itemId
 * @param {function} cb
 */
function findTopByItem(itemId, limit, cb) {
  if(typeof limit === 'function') {
    cb = limit;
    limit = 1;
  }

  var t = Database.db
    .select('bids')
    .where({itemId: itemId})
    .orderBy('date')
    .reverse()
    .limit(limit)
    .exec(Bid.populate.bind(null, function(err, rows) {
      if(err) {
        return cb(err);
      }

      if(limit === 1) {
        rows = rows[0];
      }

      cb(null, rows);
    }));
}

/**
 * Funds bid rows by user
 * @param {string} userId
 * @param {function} cb
 */
function findByUser(userId, cb) {
  Database.db
    .select('bids')
    .where({userId: userId})
    .exec(Bid.populate.bind(null, cb));
}

/**
 * Populates bid userId itemId
 * @param {function} cb
 * @param {object} err
 * @param {array | object} rows
 */
function populate(cb, err, rows) {
  if(err) {
    return cb(err);
  }

  rows = rows.map(function(row) {
    return {
      id: row.get('id'),
      userId: row.get('userId'),
      itemId: row.get('itemId'),
      amount: row.get('amount'),
      date: row.get('date')
    };
  });

  async.each(rows, function(row, done) {
    async.series({
      user: User.findById.bind(null, row.userId),
      item: Item.findById.bind(null, row.itemId)
    }, function(err, results) {
      if(err) {
        return done(err);
      }

      row.user = results.user;
      row.item = results.item;

      done();
    });
  }, function(err) {
    if(err) {
      return cb(err);
    }

    cb(null, rows);
  });
}

module.exports = Bid;
