/**
 * @module arveres/system
 */

'use strict';

var CronJob = require('cron').CronJob;
var mailer = require('nodemailer');
var async = require('async');

var emailConfig = require('./config/email');
var Auction = require('./dao/auction');
var Keyword = require('./dao/keyword');
var Win = require('./dao/win');
var Notification = require('./dao/notification');

/**
 * Creates an instance of SystemController
 * @constructor
 */
function SystemController() {
  var self = this;

  self.jobs = {};
  self.transport = mailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'arveres.auctions@gmail.com',
      pass: 'thepassword'
    }
  }, {
    from: 'arveres.auctions@gmail.com'
  });
}

/**
 * Creates and adds a job to the SystemController
 * @param {string} auctionId
 * @param {object} date
 */
SystemController.prototype.createJob = function(auctionId, date) {
  var self = this;

  if(self.jobs[auctionId]) {
    throw new Error('Auction ID ' + auctionId + ' already exists');
  }

  var job = new CronJob(new Date(date), self._processJob.bind(self, auctionId),
                        self._deleteJob.bind(self, auctionId), true);

  self.jobs[auctionId] = job;
};

/**
 * Creates and adds a notification job to the SystemController
 * @param {string} notId
 */
SystemController.prototype.createNotJob = function(notId, cb) {
  var self = this;

  if(self.jobs[notId]) {
    throw new Error('Notification ID ' + notId + ' already exists');
  }

  var time = Date.now() + (1* 60000);

  var job = new CronJob(new Date(time), self._deleteNotJob.bind(self, notId),
                        self._deleteNotJob.bind(self, notId), true);

  self.jobs[notId] = job;

  if(cb) {
    cb();
  }
}

/**
 * Deletes a job from the SystemController
 * @param {string} auctionId
 */
SystemController.prototype.deleteJob = function(auctionId) {
  var self = this;

  if(!self.jobs[auctionId]) {
    throw new Error('Auction ID ' + auctionId + ' does not exist');
  }

  self.jobs[auctionId].stop();
};

/**
 * Deletes a notification job from this SystemController
 * @param {string} notId
 */
SystemController.prototype._deleteNotJob = function(notId) {
  var self = this;

  Notification.deleteById(notId, function(err) {
    if(err) {
      console.log(err);
    }
  });

  delete self.jobs[notId];
}

/**
 * Processes a completed auction
 * @param {string} auctionId
 */
SystemController.prototype._processJob = function(auctionId) {
  var self = this;

  Auction.findById(auctionId, function(err, auction) {
    if(err) {
      console.log(err);
    }

    var subject;
    var text;

    if(auction.bid) {
      Win.create(auction.bid.user.id, auction.seller.id,
                 auction.item.id, function(err) {
        if(err) {
          console.log(err);
        }
      });

      var mailOptions = {
        to: auction.bid.user.email,
        subject: emailConfig.win.buyer.subject,
        text: emailConfig.win.buyer.body(auction.bid.user.name,
                                         auction.item.title)
      };

      self.transport.sendMail(mailOptions, function(err, res) {
        if(err) {
          return console.log(err);
        }
      });

      subject = emailConfig.win.seller.subject;
      text = emailConfig.win.seller.body(auction.seller.name,
                                         auction.item.name);
    } else {
      subject = emailConfig.lose.subject,
      text = emailConfig.lose.body(auction.seller.name,
                                   auction.item.title);
    }

    var mailOptions = {
      to: auction.seller.email,
      subject: subject,
      text: text
    };

    self.transport.sendMail(mailOptions, function(err, res) {
      if(err) {
        return console.log(err);
      }
    });

    self._deleteJob(auctionId);
  });
};

/**
 * Deletes a job from the SystemController
 * @param {string} auctionId
 */
SystemController.prototype._deleteJob = function(auctionId) {
  var self = this;

  async.series([
    Auction.deleteById.bind(null, auctionId),
    Auction.deleteSubscriptionByAuction.bind(null, auctionId),
    Keyword.deleteLinkByAuction.bind(null, auctionId)
  ], function(err) {
    if(err) {
      console.log(err);
    }
  });

  delete self.jobs[auctionId];
};

module.exports = new SystemController();