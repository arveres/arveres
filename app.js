/**
 * @module arveres/app
 */

'use strict';

// basic express modules
var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');

// view engine modules
var exphbs = require('express-handlebars');

// authentication
var passport = require('passport');
var session = require('express-session');
var flash = require('connect-flash');

// handlers
var ErrorHandler = require('./handlers/error');
var IndexHandler = require('./handlers/index');
var AuctionsHandler = require('./handlers/auctions');
var UserHandler = require('./handlers/user');

//configure passport
var configurePassport = require('./config/passport');

// file upload
var multer = require('multer');

var app = express();

// configure multer
app.use(multer({ dest: './public/imgs/',
    rename: function (fieldname, filename) {
        return filename+Date.now();
    },
    onFileUploadStart: function (file) {
        console.log(file.originalname + ' is starting ...');
    },
    onFileUploadComplete: function (file) {
        console.log(file.fieldname + ' uploaded to  ' + file.path)
    }
}).single('picture'));

// set view engine
var hbs = exphbs.create({
  defaultLayout: 'layout',
  extname: '.html',

  helpers: {
    ifCond: function(v1, v2, options) {
      if(v1 && v2 && v1 === v2) {
        return options.fn(this);
      }

      return options.inverse(this);
    },
    date: function(v1){
      if(v1) {
        var date = new Date(Number(v1));
        return date.toLocaleDateString();
      }
      
      return 'No Date';
    },
    time: function(v1) {
      if(v1) {
        var date = new Date(Number(v1));
        return date.toLocaleTimeString();
      }
      
      return 'No Time';
    }
  }
});

app.set('views', path.join(__dirname, 'views'));
app.engine('html', hbs.engine);
app.set('view engine', '.html');

// set basic properties
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/auctions', express.static(path.join(__dirname, 'public')));
app.use('/user', express.static(path.join(__dirname, 'public')));
app.use('/auctions/search', express.static(path.join(__dirname, 'public')));

// set passport
app.use(session({
  secret: 'whysosalty',
  saveUninitialized: true,
  resave: true
}));

app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

configurePassport(passport);
// routers
var indexRouter = require('./routes/index')(IndexHandler, passport);
var auctionsRouter = require('./routes/auctions')(AuctionsHandler);
var userRouter = require('./routes/user')(UserHandler);

// set routes
app.use('/', indexRouter);
app.use('/auctions', auctionsRouter);//auctions
app.use('/user', userRouter);

// set error handlers
app.use(ErrorHandler.pageNotFound);
app.use(ErrorHandler.handlePageError);

module.exports = app;
